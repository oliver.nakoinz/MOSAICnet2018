---
title: "Einführung in R"
author: '[PD Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
date: "WS 2018/2019
<br> 
[![License: CC BY-SA 4.0](../5figures/icons/License-CC BY--SA 4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)"
bibliography: ../8lib/lit.bib
csl: ../8lib/RGK_archaeology_DGUF.csl
lang: en-GB
otherlangs: de-DE
output:
  slidy_presentation:
    highlight: tango
    pandoc_args:
    - --css
    - stycss/styles.css
    footer: "Oliver Nakoinz"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
Comment: https://www.rdocumentation.org/packages/rmarkdown/versions/1.8/topics/slidy_presentation
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      message = FALSE,
                      warning = FALSE)
```

# Test

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Olat: xxx
</div>



# Test, Frage 1: 

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Frage 1: 
</div>

# Test, Frage 2: 

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Frage 2: 
</div>

# Test, Frage 3: 

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Frage 3: 
</div>


# Aufgabe

1. Richte ein RStudio Projekt ein
2. Erstelle mit Rstudio ein R-Notebook (= RMarkdowndatei integriert in RStudio)
3. Gestalte das Dokument als persönliches R-Tutorial. 
    - entscheide über die Inhalte (wähle beispielsweise aus den Präsentieren Inhalten aus udn variiere diese)
    - Schreibe Erläuterungen die Dir hilfreich sind
    - Dieses Dokument dient ggf. als Prüfungsleistung
    
# R als Rechner: Operatoren

(Teste diese Funktionalität mit einem Code-Block im R-Notebook)


```{r}
5+3
5-3
5*3
5/3
5^3
```

# R als Rechner: Operatoren

(Teste diese Funktionalität mit einem Code-Block im R-Notebook)

```{r}
4 == 6
4 != 6
4 < 6
4 > 6
(4 < 6) | (4 > 6)
(4 < 6) & (4 > 6)
```

# R als Rechner: Funktionen

(Teste diese Funktionalität mit einem einfachen R-Skript)

```{r}
sqrt(25)
sin(3.14)
log(25)
log10(25)
abs(-345.3356)
round(2.43135)
ceiling(2.5)
floor(2.5)
```

# Code analysieren

![](../5figures/codeInterpret_v01.png)

# R als Rechner: noch einige Funktionen

(Teste diese Funktionalität mit einem einfachen R-Skript)

```{r eval=FALSE, include=TRUE}
sin(3.14)
sin(3,14)
```

# R-Objekte: Variablen

```{r Variablen}
x <- 5
x
sqrt(x)
y <- 7
5 -> x
```

# Arbeitsverzeichnis

```{r}
wd_lin <- "/home/xxxx/xxxx"
wd_win <- "D:\\xxx\\xxxx"
# set(wd_lin)
```

RStudio Projekte setzen das Arbeitsverzeichnis automatisch. Wird ohne Projekte gearbeitet muss es manuell gesetzt werden

# Datentypen

<div class="column-left">
- numerical
- logical
- complex
- character
- raw
- date
</div>

<div class="column-right">
```{r}
22
TRUE
2i+2
"Text"
as.raw(65)
as.Date("2018-11-15")
```
</div>

# Datenstrukturen

- Vektor
- Matrix
- Array
- DataFrame
- Faktor
- Liste

# Datenstrukturen: Vektoren

<div class="column-left">
> Ein Vektor enthält eine geordnete Menge von Elementen.

Vektoren werden mit der Funktion `c` definiert

```{r}
x <- c(3,5,2,2,5,8,5,2)
y <- c(6,7,2,4,5,2,9,1)

```
</div>

<div class="column-right">
![Vektor](../5figures/vector.png)
</div>

# Datenstrukturen: Vektoren

<div class="column-left">
Vektoren lassen sich mit Hilfe von Sequenzen definieren und mit Funktionen manipulieren.
</div>

<div class="column-right">
```{r}
z <- seq(1:5)
z2 <- seq(along=y)
z3 <- rep(3,5)
x_sort <- sort(x)
x_rev <- rev(x)
x_revsort <- rev(sort(x))
x_sortdec <- sort(x, decreasing = T)
```

</div>

# Datenstrukturen: Vektoren

<div class="column-left">
Indizees erlauben Teile von Vektoren (und anderen Strukturen) anzusprechen.
</div>

<div class="column-right">
```{r}
x[4]
x[c(2,4)]
x[3:6]
x[x>4]
```
</div>


# Datenstrukturen: Vektoren

<div class="column-left">
Vektoren lassen sich mit Hilfe von Funktionen auswerten. 
</div>

<div class="column-right">
```{r}
min(x)
range(x)
length(x)
```
</div>



# Matrizen

- zweidimensional
- keine Zeilen oder Spaltennamen

```{r}
zx <- matrix(x,2,4)
zy <- matrix(y,2,4)
zx+zy
```

Prüfe diesen Zeie zusätzlich in einem R-Skript oder einfach in der Konsole: *zx$x*

# Array

- mehrdimensional
- Matrixalgebra möglich

```{r}
a <- array(data = 1:12,
dim = c(3, 2, 2) )
a
```

# Dataframe

- Tabelle mit Spalten- und Zeilennamen

```{r}
df <- data.frame(x,y)
df$x
df[,1]
df[1,]
which(x == 2)
```

# Faktoren

- nominale Variablen werden zahlencodiert

```{r}
f <- factor (c("Nicole", "Sabire",
"Clemens", "Jos", "Nicole"))
f
unclass(f)
levels(f)
```


# Listen

- Container für alles mögliche
- Doppelklammer index

```{r}
l <- list(c("Nicole", "Sabire","Clemens", "Jos"), as.Date("2018-11-15"), x)
l[[1]][2]
```


# Funktionen

Funktionen bündeln Code zu Algorithmen die mit einer Codezeile aufgerufen werden können. Dies kann neben R Code auch beispielswiese C++ Code sein. 

Funktionen werden in Base R oder in Paketen geliefert und können auch selbst definiert werden.

```{r}
sin(3)
mean(x)
rbind(x,y)
cbind(x,y)
rnorm(5, mean=7, sd=1)
```

# Die wichtigsten Funktionen in R

<div class="column-left-small">
![](../5figures/icons/iconmonstr-lifebuoy-3.png)
</div>

<div class="column-right-big">
```{r}
help(sin)
?rbind
# shortcut for 'help()'
??read
summary(x)
str(x)
ls()
dir()
```
</div>


# Übung

<div class="column-left-small">
![](../5figures/icons/iconmonstr-time-15.png)
</div>

<div class="column-right-big">
1. xxxxx
2. xxxxxxx
3. xxxxxxxxxxx
</div>



# xxxxxxxxxxx
