---
title: "Einführung in R"
author: '[PD Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
date: "WS 2018/2019
<br> 
[![License: CC BY-SA 4.0](../5figures/icons/License-CC BY--SA 4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)"
bibliography: ../8lib/lit.bib
csl: ../8lib/RGK_archaeology_DGUF.csl
lang: en-GB
otherlangs: de-DE
output:
  slidy_presentation:
    highlight: tango
    pandoc_args:
    - --css
    - stycss/styles.css
    footer: "Oliver Nakoinz"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
Comment: https://www.rdocumentation.org/packages/rmarkdown/versions/1.8/topics/slidy_presentation
---



# Test

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Olat: xxx
</div>



# Test, Frage 1: 

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Frage 1: 
</div>

# Test, Frage 2: 

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Frage 2: 
</div>

# Test, Frage 3: 

<div class="column-left-small">
![](../5figures/icons/iconmonstr-help-3t.png)
</div>

<div class="column-right-big">
Frage 3: 
</div>


# Aufgabe

1. Richte ein RStudio Projekt ein
2. Erstelle mit Rstudio ein R-Notebook (= RMarkdowndatei integriert in RStudio)
3. Gestalte das Dokument als persönliches R-Tutorial. 
    - entscheide über die Inhalte (wähle beispielsweise aus den Präsentieren Inhalten aus udn variiere diese)
    - Schreibe Erläuterungen die Dir hilfreich sind
    - Dieses Dokument dient ggf. als Prüfungsleistung
    
# R als Rechner: Operatoren

(Teste diese Funktionalität mit einem Code-Block im R-Notebook)



```r
5+3
```

```
## [1] 8
```

```r
5-3
```

```
## [1] 2
```

```r
5*3
```

```
## [1] 15
```

```r
5/3
```

```
## [1] 1.666667
```

```r
5^3
```

```
## [1] 125
```

# R als Rechner: Operatoren

(Teste diese Funktionalität mit einem Code-Block im R-Notebook)


```r
4 == 6
```

```
## [1] FALSE
```

```r
4 != 6
```

```
## [1] TRUE
```

```r
4 < 6
```

```
## [1] TRUE
```

```r
4 > 6
```

```
## [1] FALSE
```

```r
(4 < 6) | (4 > 6)
```

```
## [1] TRUE
```

```r
(4 < 6) & (4 > 6)
```

```
## [1] FALSE
```

# R als Rechner: Funktionen

(Teste diese Funktionalität mit einem einfachen R-Skript)


```r
sqrt(25)
```

```
## [1] 5
```

```r
sin(3.14)
```

```
## [1] 0.001592653
```

```r
log(25)
```

```
## [1] 3.218876
```

```r
log10(25)
```

```
## [1] 1.39794
```

```r
abs(-345.3356)
```

```
## [1] 345.3356
```

```r
round(2.43135)
```

```
## [1] 2
```

```r
ceiling(2.5)
```

```
## [1] 3
```

```r
floor(2.5)
```

```
## [1] 2
```

# Code analysieren

![](../5figures/codeInterpret_v01.png)

# R als Rechner: noch einige Funktionen

(Teste diese Funktionalität mit einem einfachen R-Skript)


```r
sin(3.14)
sin(3,14)
```

# R-Objekte: Variablen


```r
x <- 5
x
```

```
## [1] 5
```

```r
sqrt(x)
```

```
## [1] 2.236068
```

```r
y <- 7
5 -> x
```

# Arbeitsverzeichnis


```r
wd_lin <- "/home/xxxx/xxxx"
wd_win <- "D:\\xxx\\xxxx"
# set(wd_lin)
```

RStudio Projekte setzen das Arbeitsverzeichnis automatisch. Wird ohne Projekte gearbeitet muss es manuell gesetzt werden

# Datentypen

<div class="column-left">
- numerical
- logical
- complex
- character
- raw
- date
</div>

<div class="column-right">

```r
22
```

```
## [1] 22
```

```r
TRUE
```

```
## [1] TRUE
```

```r
2i+2
```

```
## [1] 2+2i
```

```r
"Text"
```

```
## [1] "Text"
```

```r
as.raw(65)
```

```
## [1] 41
```

```r
as.Date("2018-11-15")
```

```
## [1] "2018-11-15"
```
</div>

# Datenstrukturen

- Vektor
- Matrix
- Array
- DataFrame
- Faktor
- Liste

# Datenstrukturen: Vektoren

<div class="column-left">
> Ein Vektor enthält eine geordnete Menge von Elementen.

Vektoren werden mit der Funktion `c` definiert


```r
x <- c(3,5,2,2,5,8,5,2)
y <- c(6,7,2,4,5,2,9,1)
```
</div>

<div class="column-right">
![Vektor](../5figures/vector.png)
</div>

# Datenstrukturen: Vektoren

<div class="column-left">
Vektoren lassen sich mit Hilfe von Sequenzen definieren und mit Funktionen manipulieren.
</div>

<div class="column-right">

```r
z <- seq(1:5)
z2 <- seq(along=y)
z3 <- rep(3,5)
x_sort <- sort(x)
x_rev <- rev(x)
x_revsort <- rev(sort(x))
x_sortdec <- sort(x, decreasing = T)
```

</div>

# Datenstrukturen: Vektoren

<div class="column-left">
Indizees erlauben Teile von Vektoren (und anderen Strukturen) anzusprechen.
</div>

<div class="column-right">

```r
x[4]
```

```
## [1] 2
```

```r
x[c(2,4)]
```

```
## [1] 5 2
```

```r
x[3:6]
```

```
## [1] 2 2 5 8
```

```r
x[x>4]
```

```
## [1] 5 5 8 5
```
</div>


# Datenstrukturen: Vektoren

<div class="column-left">
Vektoren lassen sich mit Hilfe von Funktionen auswerten. 
</div>

<div class="column-right">

```r
min(x)
```

```
## [1] 2
```

```r
range(x)
```

```
## [1] 2 8
```

```r
length(x)
```

```
## [1] 8
```
</div>



# Matrizen

- zweidimensional
- keine Zeilen oder Spaltennamen


```r
zx <- matrix(x,2,4)
zy <- matrix(y,2,4)
zx+zy
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    9    4   10   14
## [2,]   12    6   10    3
```

Prüfe diesen Zeie zusätzlich in einem R-Skript oder einfach in der Konsole: *zx$x*

# Array

- mehrdimensional
- Matrixalgebra möglich


```r
a <- array(data = 1:12,
dim = c(3, 2, 2) )
a
```

```
## , , 1
## 
##      [,1] [,2]
## [1,]    1    4
## [2,]    2    5
## [3,]    3    6
## 
## , , 2
## 
##      [,1] [,2]
## [1,]    7   10
## [2,]    8   11
## [3,]    9   12
```

# Dataframe

- Tabelle mit Spalten- und Zeilennamen


```r
df <- data.frame(x,y)
df$x
```

```
## [1] 3 5 2 2 5 8 5 2
```

```r
df[,1]
```

```
## [1] 3 5 2 2 5 8 5 2
```

```r
df[1,]
```

```
##   x y
## 1 3 6
```

```r
which(x == 2)
```

```
## [1] 3 4 8
```

# Faktoren

- nominale Variablen werden zahlencodiert


```r
f <- factor (c("Nicole", "Sabire",
"Clemens", "Jos", "Nicole"))
f
```

```
## [1] Nicole  Sabire  Clemens Jos     Nicole 
## Levels: Clemens Jos Nicole Sabire
```

```r
unclass(f)
```

```
## [1] 3 4 1 2 3
## attr(,"levels")
## [1] "Clemens" "Jos"     "Nicole"  "Sabire"
```

```r
levels(f)
```

```
## [1] "Clemens" "Jos"     "Nicole"  "Sabire"
```


# Listen

- Container für alles mögliche
- Doppelklammer index


```r
l <- list(c("Nicole", "Sabire","Clemens", "Jos"), as.Date("2018-11-15"), x)
l[[1]][2]
```

```
## [1] "Sabire"
```


# Funktionen

Funktionen bündeln Code zu Algorithmen die mit einer Codezeile aufgerufen werden können. Dies kann neben R Code auch beispielswiese C++ Code sein. 

Funktionen werden in Base R oder in Paketen geliefert und können auch selbst definiert werden.


```r
sin(3)
```

```
## [1] 0.14112
```

```r
mean(x)
```

```
## [1] 4
```

```r
rbind(x,y)
```

```
##   [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8]
## x    3    5    2    2    5    8    5    2
## y    6    7    2    4    5    2    9    1
```

```r
cbind(x,y)
```

```
##      x y
## [1,] 3 6
## [2,] 5 7
## [3,] 2 2
## [4,] 2 4
## [5,] 5 5
## [6,] 8 2
## [7,] 5 9
## [8,] 2 1
```

```r
rnorm(5, mean=7, sd=1)
```

```
## [1] 6.869443 5.514427 8.855633 7.989541 5.893280
```

# Die wichtigsten Funktionen in R

<div class="column-left-small">
![](../5figures/icons/iconmonstr-lifebuoy-3.png)
</div>

<div class="column-right-big">

```r
help(sin)
?rbind
# shortcut for 'help()'
??read
summary(x)
```

```
##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
##       2       2       4       4       5       8
```

```r
str(x)
```

```
##  num [1:8] 3 5 2 2 5 8 5 2
```

```r
ls()
```

```
##  [1] "a"         "df"        "f"         "l"         "wd_lin"   
##  [6] "wd_win"    "x"         "x_rev"     "x_revsort" "x_sort"   
## [11] "x_sortdec" "y"         "z"         "z2"        "z3"       
## [16] "zx"        "zy"
```

```r
dir()
```

```
## [1] "B0_Rintro1.html"          "B0_Rintro1.Rmd"          
## [3] "B0_Rintro2.Rmd"           "PresentationExample.html"
## [5] "PresentationExample.Rmd"  "slidy_xxxtemplate.Rmd"   
## [7] "stycss"
```
</div>


# Übung

<div class="column-left-small">
![](../5figures/icons/iconmonstr-time-15.png)
</div>

<div class="column-right-big">
1. xxxxx
2. xxxxxxx
3. xxxxxxxxxxx
</div>



# xxxxxxxxxxx
