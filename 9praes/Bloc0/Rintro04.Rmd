---
title: "Introdution to R"
subtitle: "R and Spatial Data"
author: '[Franziska Faupel, Clara Filet & PD Dr. Oliver Nakoinz](https://ffaupel.gitlab.io/mosaic/)'
date: "October 2018
<br> 
[![License: CC BY-SA 4.0](../../5figures/icons/License-CC BY--SA 4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
"
bibliography: ../../8lib/lit.bib
csl: ../../8lib/RGK_archaeology_DGUF.csl
lang: de-DE 
otherlangs: en-GB
output:
  slidy_presentation:
    highlight: tango
    pandoc_args:
    - --css
    - stycss/styles.css
    footer: "Franziska Faupel, Clara Filet & PD Dr. Oliver Nakoinz"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
Comment: https://www.rdocumentation.org/packages/rmarkdown/versions/1.8/topics/slidy_presentation
---

## An Introdution to R 

<div class="column-left">

- R - What is it?!
    - Why using R?
    - History of R
    - 7 Levels of Learning
    - R Studio
    - Scripts

- Working with R
    - Function vs Objects
    - Vectors
    - Functions
    - Operators
    - Data Frames and Matrices

</div>

<div class="column-right">

- First Calculations and Plots in R
    - Package Management
    - Loading Data
    - Data Management
    - Loops and Restrictions
    - Some Statistics
    - Plotting in R
    

<font color="#336600">

- R and Spatial Data
    - Load Spatial Objects
    - Create Spatial Objects
    - Plot Spatial Objects
    
</font>
    
</div>

---

## Overview 


```{r echo=FALSE, results='asis'}
indices <- read.csv("../../2data/tables/SpatialObjects.csv", sep = ",", header = TRUE)
xtable::xtable(indices, align = c("l","l", "l", "l","l", "l"))
```

[//]: # (R Studio: --> Script_RGIS_PRES.R)
