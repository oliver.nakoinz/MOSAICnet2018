################################################################################
## R Script to thest wheater the functions work proberly
## =============================================================================
## Author of Functions: Clemens Schmid & Franziska Faupel
## Version: 05
## Date of last changes: 18.09.2018
## Licence Script: GPL (http://www.gnu.org/licenses/gpl-3.0.html)
################################################################################

source("9praes/Bloc 3/FUN_rePath.R")

testmatrix <- data.frame(
  x = abs(rnorm(100)*50), 
  y = abs(rnorm(100)*50)
)

plot(testmatrix$x, testmatrix$y)


xmin    <- 0
xmax    <- max(testmatrix$x)
ymin    <- 0
ymax    <- max(testmatrix$y)
ext_ai <- raster::extent(xmin, xmax, ymin, ymax)

sv <- (xmax-xmin)/(ymax-ymin)

rw     <- 10   # width of raster defined in m

rows  <- round((ymax-ymin)/rw, 0) + 1 ; rows                                    #Anzahl der Zeilen
colums <- round((xmax-xmin)/rw, 0) + 1 ; colums                                     #Anzahl der Spalten
v <- cbind(1:(colums*rows))                                              #Vektor fÃÂ¼r alle Gridzellen
df <- data.frame(v)                                                         #Konvertiert den Vektor zu einem Dataframe
gt <- sp::GridTopology(c(xmin, ymin), c(rw, rw), c(colums, rows))           
sgdf <- sp::SpatialGridDataFrame(gt, df) # Grid wird projeziert


bb   <- sp::bbox(sgdf)
win  <- spatstat::owin(xrange=c(bb[1,1],bb[1,2]), yrange=c(bb[2,1],bb[2,2]), unitname="m")
# spdf_g <- remove.duplicates(spdf_g, zero=0, remove.second=TRUE)

spdf_g <- sp::SpatialPoints(testmatrix)
ppp_g <- spatstat::ppp(spdf_g@coords[,1], spdf_g@coords[,2], window=win)
g_ai <- data.frame(ppp_g)

f_sd1  <- 4

nn <- mean(spatstat::nndist(ppp_g))
sd1 <- sd1  <- f_sd1*nn

base_kde <- makestatkde(sd1 = sd1, sgdf = sgdf, df = testmatrix)
 

iter <- 2
rw     <- 500   # width of raster defined in m
tresh  <- 0.05  # treshold, to delete phaths in areas with really low density values (KDE), meaning calculation artefacts 
f_sd1  <- 4     # factor defining size if the first kernel, which generate the stucture of dynamic kernel
f1     <- 0.2      # factor defining the minimum border of dynamic kernel (raster width) f1*mean(nn)  ## 0.2
f2     <- 0.4      # factor defining the maximum border of dynamic kernel f2*mean(nn)
f3     <- 0.5      # MinimalinentitÃÂ¤t des Kernels
f4     <- 1        # MaximalinentitÃÂ¤t des Kernels
s      <- -0.3     # Kernelparameter: incline starting from ponit 1
de     <- 0.7      # hight of additional kernell peak
sw     <- 12       # width of picture, cm
mwin   <- 9        # Mowing-window-size for ridge detection (4,9,16)
xp <- 750     # Kernelparameter: x-wert   von Punkt 1

PATH <- repath(base_kde, nn, sgdf)

