---
title: "SNA - Theory"
author: '[PD Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
date: "October 2018
<br> 
[![License: CC BY-SA 4.0](../../5figures/icons/License-CC BY--SA 4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
"
bibliography: ../../8lib/lit.bib
csl: ../../8lib/RGK_archaeology_DGUF.csl
lang: en-GB
otherlangs: de-DE
output:
  slidy_presentation:
    highlight: tango
    pandoc_args:
    - --css
    - ../dummy/stycss/styles.css
    footer: "Oliver Nakoinz"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
Comment: xxxxxxxxxxxxxxxxxxxxxx
#duration: 30
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      message = FALSE,
                      warning = FALSE)
```


# History of research

- Auguste Comte (1798-1857): sociology as a science
- Alexander Macfarlane (1851-1913): graphics and mathematics
- Georg Simmel (1858-1918): focus on interaction
- Jacob Moreno (1889-1974): sociometry, term network (1934)
- Harrison White (1930-): mathematisation and "Harvard Breakthrough" 
- Linton Freemann (1927-2018): centrality, *Social Networks* Journal
- Mark Granovetter (1943): weak ties


# Graphs

- Graphs are mathematical structures modelling the real world using only:
    - nodes = vertices = points
    - edges = lines
- Networks are a formalized (=mathematical) abstraction of real world phenomena.
- Networks are models

# Models: Definition

> Models are artefacts representing something for a certain purpose and used by a certain community of practice

# Models: Thalheim House

![](../../5figures/modelhouse.svg)

# Models: Types

<div class="column-left">
![](../../5figures/modell_en_v04.svg)
</div>

<div class="column-right">
![](../../5figures/modell_vergl_en_v04.svg)
</div>


# Graphs
![](../../5figures/graphen.png)

# Paradigms

![](../../5figures/netzparadigma.svg)

# Complementarity

![](../../5figures/KolNetKomp_engl_v02.svg)

# Complementarity

<div class="column-1-3">
**Network**

- few edges
- focusing on (individual) connections

</div>

<div class="column-2-3">

**Matrix**

- many edges
- differentiated values
- focus on (the system of) relationships

</div>

<div class="column-3-3">
**Group**

- edges not necessarily clear defined
- focusing on in/out, member/not member
- delimitation

</div>

# The whole picture

![](../../5figures/KulturNetzDreieck_engl_v03.svg) 


# Network vs Graph

- Graphs are mathematical structure
- Networks are objects  in actual research contexts based on graphs
- Networks sometimes are less restrictive (multiple edges allowed)


# Network Analysis

- Managing data
- Visualising networks
- Analysing networks
- (Modelling/simulating networks)

# Network Analysis

- "importance"/prominence of nodes
    - node centrality
- "importance" of edges
    - edge centrality
- groups of nodes
    - subgroups
    - cluster
- "groups" of edges
    - shortest path
- distances

# Centrality

![](../../5figures/tzi_schema.svg)


- eigenvector centrality
- alpha centrality
- ...

# Subgroups / Cliques

Cliques are complete sub-graphs. 

- clique: all nodes are connected to each other
- n-clique: all nodes are connected to each other via maximal n nodes
- n-clan: all nodes are connected to each other via maximal n nodes being members of the clan
- k-plex: all notes are connected to all but k other nodes
- k-core: all notes are connected to k or more other nodes

# Partitions/Communities

> Cluster are groups of elements with similar features.

Cluster analysis:

1. calculation of distance matrix
    - which elements and features (nodes and edges)?
    - metric (Euclidean, Manhattan, Jaccard, ...)
2. fusion of elements
    -  partitioning approaches
        - k-means
        - ...
    - hierarchical approaches
            - single linkage
            - average linkage
            - centroid
            - ...
        - density based approaches
        - fuzzy clustering
        - ...

# Shortest path

Dijkstra Algorithm

1. mark starting point and set starting costs
2. each for the edge from a marked point to a not marked point which, when added to the costs produces the minimal costs
3. calculate the new cost by adding the edge weight to the costs
4. note the step and mark the new point
5. repeat step 2 and 4 until the target is reached 


# References

Some classical references: @borgatti_2013; @Freemann2004; @Grano1973; @Hanneman2005; @MF1883; @Moreno1934; @Scott1992; @Wasser1994; @White1970
