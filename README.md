[**MOSAICnet**](https://www.ufg.uni-kiel.de/de/aktuelles/events/tagungen-ausstellungen/mosaic2018) - Networks in Archaeological Research

Research School AOROC - Bibracte 4

International ‘Ecole Thématique’ in partnership with the Franco-German University

## Topic

Ranging from exchange to urban networks, interactions systems are a basic component of every society, from the prehistoric to the modern. The Research School ‘MOSAICnet: Networks in archaeological research’ aims at bringing together young and senior researchers around this transdisciplinary issue of networks. The Research School aimes at bringing together the state of the art of several methodological approaches (such as Social Network Analysis, Spatial Network Analysis, etc.), resulting from the dialogue between the Human and the Exact Sciences. It also intends to discuss their contributions to the research on past social, cultural and economic interactions. The Research School will last 6 days, and will address the matters of the theoretical context of the research on interaction systems, the empirical data we can rely on to grasp them, as well as provide an overview of several available methods of system analysis and reconstruction. Those tools will be applied using the Rstudio software. 

---

## Organisation

### Institutes

- Bibracte European Research Centre
- Laboratoire AOROC ENS Paris
- Université Franco-Allemande / Deutsch-Französische Hochschule
- CNRS
- Institute of Pre- and Protohistoric Archaeology
- Johanna Mestorf Academy - Kiel University

### Organisation Committee

- Katherine Gruel
- Oliver Nakoinz
- Vincent Guichard
- Clara Filet 
- Franziska Faupel

### Scientific Committee

- Oliver Nakoinz, Privat-docent, Institute of Pre- and Protohistoric Archaeology, Kiel University, Germany
- Katherine Gruel, Research Director CNRS Ecole Normale Supérieure, France
- Clara Filet, PhD candidate Paris 1 University, France
- Franziska Faupel, PhD candidate Kiel University, Germany

### Guest Lecturers

- Olivier Buchsenschutz, Emeritus Research Director CNRS, Ecole Normale Supérieure, France
- Patrice Brun, University Professor Paris 1 University, France
- Fabrice Rossi, University Professor Paris 1 University, France

---

## Program and Material

(it is intended to have the program and links to the presentation, data and other material here)

### Monday 8

Block 0 : Introduction to R and Rstudio (O. Nakoinz, C. Filet and F. Faupel) An introductory course on the use of the R language, intended for participants who have no practical experience of this programming language and of the use of scripts.

- 14:00-18:30
    - Introduction to R Part 1: Concept and Philosophy of R
        - [Introduction in R Part 1 rmd](9praes/Bloc0/Rintro01.Rmd) 
        - [Introduction in R Part 1 html](9praes/Bloc0/Rintro01.html) 
        - [Introduction in R Part 1 rendered html](http://picayune-fish.surge.sh/Rintro01.html)  to be updated
    - Introduction to R Part 2: Working with R
        - [Introduction in R Part 2 rmd](9praes/Bloc0/Rintro01.Rmd) 
        - [Introduction in R Part 2 html](9praes/Bloc0/Rintro01.html) 
        - [Introduction in R Part 2 rendered html](http://picayune-fish.surge.sh/Rintro01.html)  to be updated
    - Introduction to R Part 3: First Calculations and Plots in R
        - [Introduction in R Part 3 rmd](9praes/Bloc0/Rintro01.Rmd) 
        - [Introduction in R Part 3 html](9praes/Bloc0/Rintro01.html) 
        - [Introduction in R Part 3 rendered html](http://picayune-fish.surge.sh/Rintro01.html)  to be updated
    - Introduction to R Part 4: Spatial Data
        - [Introduction in R Part 4 rmd](9praes/Bloc0/Rintro01.Rmd) 
        - [Introduction in R Part 4 html](9praes/Bloc0/Rintro01.html) 
        - [Introduction in R Part 4 rendered html](http://picayune-fish.surge.sh/Rintro01.html)  to be updated

### Tuesday 9

Block 0 to be continued in the morning

Block 1 : Networks in Archaeology: considerations and challenges (O. Nakoinz, P. Brun and O. Buchsenschutz) Presentations on the theoretical context of the research on interactions and networks in past societies. Several types of networks and the means to identify and reconstruct them, as well as non-computer-based approaches on spatial interactions are considered.

- 09:00-13:00
    - Introduction to R Part 3: Spatial objects and basic statistics
- 14:00-18:00
    - Welcome and introduction (Organisation Committee)
    - Theoretical context of archaeological networks (P. Brun)
    - Archaeology of exchange systems: which data? (O. Buchsenschutz)

### Wednesday 10

Block 2 : Social Network Analysis (O. Nakoinz) Introduction to Social Network Analysis in theory and practice.  Basic concepts, the handling and visualisation of network data, and different analytical approaches are presented. Centrality indices, cliques and cluster as well as network distances are discussed. After the lectures, the discussed methods are explored by applying them to different data in different working groups.

- 09:00-13:00
    - Lecture: Social Network Analysis - Theory 
        - [gitlab rmd](sna_practice.rmd)  
        - [gitlab html](sna_practice.html)  
        - [rendered html](http://picayune-fish.surge.sh/sna_theory.html)
    - Lecture: Social Network Analysis - Practice 
        - [gitlab rmd](9praes/Bloc2/sna_theory.rmd)  
        - [gitlab html](9praes/Bloc2/sna_theory.html)  
        - [rendered html](http://picayune-fish.surge.sh/sna_practice.html)
- 14:00-18:30
    - Exercise I: Working on the problems
    - Exercise II: Applying methods to other data

### Thursday 11

Block 3 : Exchange systems in space (F. Faupel) Past interactions are constrained by the distance between actors. The course will focus on travel itineraries, by considering the analysis of the road structure, the identification of factors that influence the location of a path, and the method of least cost path analysis.

- 09:00-13:00
    - Lecture: Exchange Systems in Space (F. Faupel)
        - [Reconstruction of Paths](9praes/Bloc 3/bloc03.html)
        - [Identification of influencing parameters ](9praes/Bloc 3/bloc03b.html)
- 14:00-18:30
    - Exercise I: Reproduce presented results using provided data and script
    - Exercise II: Apply methods to own data or provide data
- Material:
    - [Data](2data/gravemounds.csv) 
    - [Geodata](3geodata/bw_srtm.asc)
    - [Exercise: Reconstructing Path Systems](1scripts/Exercise_bloc3.R)
    - [Exercise: Parameters of Infrastructure](1scripts/Exercise_bloc3b.R)
    - [Exercise: Evaluating Parameters](1scripts/Exercise_bloc3c.R)
    
### Friday 12

Block 4 : Approaching the organization of exchange systems (C. Filet, F. Rossi) Most of past interactions, their actors and their intensity are not known to archaeologists. The course will be dedicated to ways of approaching the organization of past exchange systems in the absence of written sources. For this purpose an introduction to spatial interaction models and the study of distribution patterns of archaeological artefacts will be given.

- 09:00-13:00
    - Lecture : Theory and presentation of the methods
- 14:00-18:30
    - How to do it in R
    - Workshop. Working groups
- Material:
    - xxx
    
### Saturday 13

Block 5 : Workshop and discussion. Benefits and usefulness of network analyses in archaeology.  Critical review of the applicability and limits of each method.

- 09:00-13:00
    - Workshop. Working groups
- 14:00-16:00
    - Workshop. Report of the different groups
- 16:00-18:30
    - Discussion: Critique of network analysis

---
    
## Data

- [shkr](2data/shkr_db/) comprises data from the project "Siedlungshierarchien und kulturelle Räume" and are available from the [JMA data exchange platform](https://www.jma.uni-kiel.de/en/research-projects/data-exchange-platform). There are also some selected data from this database: 
    - [some graves from the Swabian Alb](/home/fon/daten/lehre/SummerSchool/ss2018_bibracte/bibracte/2data/graves.csv)
    - [finds from the graves from the Swabian Alb](2data/personal_items.csv)
    - [list of artefacts](2data/shkr_funde.csv)
    - [fibulae](2data/fibeln.csv)
    - [table of princely seats from the Early Iron Age](2data/fs_ag.csv)
- [bibtex file with some references on Bibracte](2data/Primo_BibTeX_bibracte.bib) 
- [decorated Braubach type ceramics from Braubach](2data/keramik.csv)
- [xxx](2data/atlantdata.csv)
- [xxx](2data/AtlantData1.csv)
- [xxx](2data/AtlantData2.csv)
- [xxx](2data/AtlantData3.csv)
- [xxx](2data/AtlantPottery.csv)
- [xxx](2data/BA_SH_gravemounds.csv)
- [xxx](2data/gravemounds.csv)
- [xxx](2data/tables/)



## Additional Links:

### R

- [QAAM1](http://www.springer.com/de/book/9783319295367) 
- [QAAM1 FU](https://fu-berlin.hosted.exlibrisgroup.com/primo_library/libweb/action/display.do;jsessionid=4154B809F4EC8A6AB8C04F1CD419BDBA?tabs=detailsTab&ct=display&fn=search&doc=FUB_ALMA_DS51960803280002883&indx=2&recIds=FUB_ALMA_DS51960803280002883&recIdxs=1&elementId=1&renderMode=poppedOut&displayMode=full&frbrVersion=&pcAvailabiltyMode=false&&query=any%2Ccontains%2Cnakoinz+knitter&dscnt=0&search_scope=FUB_Blended_2&vl(1UIStartWith0)=contains&scp.scps=scope%3A%28%22FUB_SEM%22%29%2Cscope%3A%28FUB_FUEM%29%2Cscope%3A%28FUB_ALMA%29%2Cscope%3A%28%22FUB_DBIS%22%29%2Cprimo_central_multiple_fe&vid=FUB&mode=Basic&highlight=true&institution=FUB&bulkSize=10&tab=fub&displayField=all&fromLocation=CMS&vl(freeText0)=nakoinz%20knitter&dum=true&vl(277659841UI0)=any&dstmp=1518351372451)- 
- [r4ds](http://r4ds.had.co.nz/)
- [Applied Spatial Data Analysis with R](http://www.springer.com/de/book/9781461476177)
- [Applied Spatial Data Analysis with R (Datei 1. Auflage)](http://gis.humboldt.edu/OLM/r/Spatial%20Analysis%20With%20R.pdf)
- [Reproducible Research](https://link.springer.com/article/10.1007%2Fs10816-015-9272-9)
- [getting spatial data](http://www.gis-blog.com/r-raster-data-acquisition/)
- [Statistical Learning with R](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)
- [R in Action](ftp://ftp.micronet-rostov.ru/linux-support/books/programming/R/Kabacoff%20Robert%20I.%20-%20R%20in%20Action%20-%202011.pdf)
- [How-To-Do-Archaeological-Science-Using-R](https://benmarwick.github.io/How-To-Do-Archaeological-Science-Using-R/)
- [sna](https://www.jessesadler.com/post/network-analysis-with-r/)
- [sna2](http://kateto.net/networks-r-igraph)
- [sna-vis](http://pablobarbera.com/big-data-upf/html/02a-networks-intro-visualization.html)

### R Cheat Sheets etc.

- [RStudio R Base](https://www.rstudio.com/wp-content/uploads/2016/10/r-cheat-sheet-3.pdf)
- [RStudio all CheatSheets](https://www.rstudio.com/resources/cheatsheets/)
- [R short refcard](https://cran.r-project.org/doc/contrib/Short-refcard.pdf)
- [R refcard](https://cran.r-project.org/doc/contrib/Baggott-refcard-v2.pdf)
- [Cran Task View Archaeology](https://github.com/benmarwick/ctv-archaeology)

### Code-Etherpad

- [Etherpad Bibracte](https://pad.systemli.org/p/MOSAICnet)
