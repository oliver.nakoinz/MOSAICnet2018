library(moin)

context("dist_matr")

test_that("correct result", {
  mymatrix <- matrix(c("ONE", "TWO", 0,1,1,0),nrow=2)
  
  correct_result <- as.matrix(dist(mymatrix[,2:3]))
  
  rownames(correct_result) <- colnames(correct_result) <- c("ONE", "TWO")
  
  expect_equal(dist_matr(mymatrix), correct_result)
})

test_that("dumb users get error message", {
  mymatrix <- c(0,1,1,0)
  
  expect_error(dist_matr(mymatrix), "Wrong input! Matrix is expected!")
}) 

 test_that("smart users get no error message", {
   mymatrix <- matrix(c("ONE", "TWO", 0,1,1,0),nrow=2)
   
   expect_error(dist_matr(mymatrix), NA)
 })

expect