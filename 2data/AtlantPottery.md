# Beschreibung des Datensatzes `AtlantPottery.csv`

| Column      | Description                              |
| ----------- | ---------------------------------------- |
| site        | Archaeological Site                      |
| feature     | Feature                                  |
| object      | Individuum                               |
| class       | Object class                             |
| sherd       | Sherd type (Rim, Base, Wall, Complete)   |
| qty         | Quantity                                 |
| wt          | Weight (g)                               |
| size        | Size class (based on Clist 2004/05)      |
| wall        | Wall thickness (mm)                      |
| rimD        | Rim Diameter (mm)                        |
| rimH        | Hight                                    |
| minD        | Minimal diameter                         |
| minD_H      | Hight of minimal diameter                |
| maxD        | Maximal diameter                         |
| maxD_H      | Hight of maximal diameter                |
| bodenD      | Diameter of base                         |
| temperSize  | Size of temper                           |
| vesselShape | Shape of vessel                          |
  
  
## Reference

![D. Seidensticker & C. Schmid: R-Turorial beim 7. Workshop der AG CAA in Hamburg (6.-7.2.2016)](https://github.com/ISAAKiel/R-Tutorial_CAA2016) 

